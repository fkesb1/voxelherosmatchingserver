#include "Server.h"

HANDLE Server::hcp=NULL;
Server::Server(UDPSocket* udp)
{
	char command[30];
	this->udp = udp;
	CreateThread(NULL, 0, serverFunc, udp, 0, 0);
	while (true) {
		std::cin >> command;
		if (!strcmp(command, "2xit")) {
			break;
		}
	}
}
#if _WIN32

DWORD WINAPI Server::serverFunc(LPVOID args) {
	Json::Reader reader;
	Json::StyledWriter writer;
	UDPSocket *udp = (UDPSocket*)args;
	while (true) {
		Json::Value root, root2;
		WSABUF buf;
		char data[1024] = "";
		buf.buf = data;
		buf.len = 1024;
		SOCKADDR_IN sockaddr;
		int size = udp->recvFrom(&buf, &sockaddr);
		if (size > 0) {
			if (!reader.parse(buf.buf, root)) {
				std::cout << "ERROR PARSE PACKET" << std::endl;
				continue;
			}
			if (root["packetType"].asInt() == PacketType::OP_CreateRoom) {
				Room *r = new Room(sockaddr, root["body"].asString());
				root2["packetType"] = PacketType::OP_CreateRoomReply;
				root2["secure"] = false;
				root2["body"] = "planetside";
				root2["key"] = "";
				udp->sendTo(&(writer.write(root2))[0u], sockaddr);
			}
			else if (root["packetType"].asInt() == PacketType::OP_FindRoom) {
				root2["packetType"] = PacketType::OP_FindRoomReply;
				root2["secure"] = false;
				root2["body"] = Room::findRoom(sockaddr);
				root2["key"] = "";
				udp->sendTo(&(writer.write(root2))[0u], sockaddr);
			}
			else if (root["packetType"].asInt() == PacketType::OP_KillRoom) {
				std::string localIp = root["body"].asString();
				root2["packetType"] = PacketType::OP_KillRoomReply;
				root2["secure"] = false;
				root2["body"] = "";
				root2["key"] = "";
				if (Room::killRoom(sockaddr, localIp) == 0) {
					udp->sendTo(&(writer.write(root2))[0u], sockaddr);
				}
				else {
					std::cout << "err" << std::endl;
					udp->sendTo(&(writer.write(root2))[0u], sockaddr);
				}
			}

		}

	}
}
#endif
Server::~Server()
{
}
