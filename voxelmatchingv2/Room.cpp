#include "Room.h"
std::vector<Room*> room_list;
Room::Room(SOCKADDR_IN addr,std::string localAddr) {
	this->addr = addr;
	this->localAddr = localAddr;
	room_list.push_back(this);
	std::cout << "Room Created" << std::endl;
}
Room::~Room() {

}
std::string Room::findRoom(SOCKADDR_IN addr) {
	for (int i = 0; i<room_list.size(); i++) {
		if (addr.sin_addr.S_un.S_addr == room_list[i]->addr.sin_addr.S_un.S_addr) {
			std::cout << "find room " << room_list[i]->localAddr << std::endl;
			return room_list[i]->localAddr;
		}
	}
	return "";
}
int Room::killRoom(SOCKADDR_IN addr, std::string localIp) {
	for (int i = 0; i<room_list.size(); i++) {
		if (addr.sin_addr.S_un.S_addr == room_list[i]->addr.sin_addr.S_un.S_addr && room_list[i]->localAddr.compare(localIp) == 0) {
			std::cout << "KILL ROOM " << room_list[i]->addr.sin_addr.S_un.S_addr << " " << room_list[i]->localAddr << std::endl;
			Room *r = room_list[i];
			room_list.erase(room_list.begin() + i);
			delete(r);
			return 0;
		}
	}
	return -1;
}