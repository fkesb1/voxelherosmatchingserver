#include "UDPSocket.h"



UDPSocket::UDPSocket(char *addr,int port,int mode)
{
#if _WIN32
	if (WSAStartup(MAKEWORD(2, 2), &wsaData)) {
		std::cout << "WSAStartup FAILED" << std::endl;
		exit(-1);
	}
	soc = socket(PF_INET, SOCK_DGRAM, 0);
#endif
	if (soc == INVALID_SOCKET) {
		std::cout << "SOCKET ERROR" << std::endl;
		exit(-1);
	}
	this->addr.sin_family = AF_INET;
	this->addr.sin_port = htons(port);
#if _WIN32
	wchar_t wtext[20];
	mbstowcs_s(NULL, wtext, addr, strlen(addr) + 1);
	LPWSTR ptr = wtext;
	InetPton(AF_INET, ptr, &(this->addr.sin_addr));
#else
	inet_pton(AF_INET, addr, &UDPSocket::addr.sin_addr);
#endif
	if (mode == 1) {
#if _WIN32
		u_long arg = 1;
		ioctlsocket(UDPSocket::soc, FIONBIO, &arg);
#else
		int flag = fcntl(UDPSocket::soc, F_GETFL, 0);
		fcntl(UDPSocket::soc, F_SETFL, flag);
#endif
	}
	if (bind(soc, (sockaddr *)&(this->addr), sizeof(this->addr)) == SOCKET_ERROR)
	{
		std::cout << "BIND ERROR" << std::endl;
		exit(-1);
	}
	std::cout << "UDP SOCKET started" << std::endl;
}


UDPSocket::~UDPSocket()
{

}

int UDPSocket::sendTo(char *data, SOCKADDR_IN addr) {
#if _WIN32
	WSABUF buf;
	buf.buf = data;
	buf.len = strlen(data);
	int re = WSASendTo(soc, &buf, 1, NULL, 0, (SOCKADDR*)&addr, sizeof(addr), NULL, 0);
	return re;
#else
	int size = sendto(soc, recv, len, 0, (SOCKADDR*)&addr, sizeof(addr));
	return size;
#endif
}
int UDPSocket::recvFrom(WSABUF *data, SOCKADDR_IN *addr) {
	int size = sizeof(clntaddr);
	DWORD recvSize;
	DWORD flag = 0;
	OVERLAPPED o;
	ZeroMemory(&o, sizeof(o));
	int re = WSARecvFrom(soc, data, 1, &recvSize, &flag, (SOCKADDR*)&clntaddr, &size, NULL, 0);
	if (re == 0) {
		*addr = clntaddr;
		return recvSize;
	}
	else {
		return -1;
	}
}