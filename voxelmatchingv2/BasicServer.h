#pragma once
#include "TCPSocket.h"
#include "json\json.h"
#include "Room.h"
#include "op_code.h"

class BasicServer
{
public:
	BasicServer(TCPSocket*);
	~BasicServer();
private:
	TCPSocket *tcp;
	static DWORD WINAPI acceptThread(LPVOID);
	static DWORD WINAPI handleData(LPVOID);
	static HANDLE hCompletionPort;
};


