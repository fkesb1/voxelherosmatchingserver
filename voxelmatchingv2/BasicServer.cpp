#include "BasicServer.h"


struct ioContext {
	SOCKET soc;
	OVERLAPPED o;
	WSABUF buffer;
	SOCKADDR_IN addr;
	int mode;
};
HANDLE BasicServer::hCompletionPort = NULL;
BasicServer::BasicServer(TCPSocket* tcp)
{
	this->tcp = tcp;
	CreateThread(NULL, 0, acceptThread, this->tcp, 0, NULL);
	while (true) {
		char str[20];
		scanf_s("%s", str, 20);
		if (!strcmp(str, "exit")) {
			break;
		}
	}
}
DWORD WINAPI BasicServer::acceptThread(LPVOID arg) {
	TCPSocket *t = (TCPSocket*)arg;
	hCompletionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	for (int i = 0; i < 2; i++) {
		CreateThread(NULL, 0, handleData,t, 0, NULL);
	}
	while (true) {
		char data[1024] = "";
		SOCKET clntSoc;
		SOCKADDR_IN clntAddr;
		ioContext *ioc;
		int addrLen=sizeof(clntAddr);
		clntSoc = t->acceptSocket(&(t->soc),&clntAddr, &addrLen);
		if (clntSoc == INVALID_SOCKET) {
			continue;
		}
		std::cout << "CLIENT CONNECTED" << std::endl;
		ioc = new ioContext;
		ioc->soc = clntSoc;
		ioc->addr = clntAddr;
		ioc->buffer.buf = data;
		ioc->buffer.len = 1024;
		ioc->mode = 0;
		memset(&(ioc->o), 0, sizeof(OVERLAPPED));
		CreateIoCompletionPort((HANDLE)ioc->soc, hCompletionPort,(ULONG_PTR)ioc, 0);
		t->recv(&ioc->soc,&(ioc->buffer),&(ioc->o));
	}
}
DWORD WINAPI BasicServer::handleData(LPVOID arg) {
	TCPSocket *t = (TCPSocket*)arg;
	DWORD bytesReceived;
	ioContext *ioc;
	ULONG_PTR key;
	DWORD flags;
	LPOVERLAPPED overlapped;

	Json::Reader reader;
	Json::StyledWriter writer;
	while (true) {
		char data[1024] = "";
		Json::Value root, root2;
		bool status=GetQueuedCompletionStatus(hCompletionPort,&bytesReceived,&key,&overlapped,INFINITE);
		ioc = (ioContext*)key;
		if (bytesReceived == 0) {
			std::cout << "CLIENT DISCONNECTED" << std::endl;
			closesocket(ioc->soc);
			delete ioc;
			continue;
		}
		if (!status) {
			closesocket(ioc->soc);
			delete ioc;
			continue;
		}

		if (ioc->mode == 0) {
			std::cout << "recv:" << ioc->buffer.buf << std::endl;
			if (!reader.parse(ioc->buffer.buf, root)) {
				std::cout << "ERROR PARSE PACKET" << std::endl;
				t->recv(&ioc->soc, &ioc->buffer, &ioc->o);
				continue;
			}
			
			if (root["packetType"].asInt() == PacketType::OP_CreateRoom) {
				Room *r = new Room(ioc->addr, root["body"].asString());
				root2["packetType"] = PacketType::OP_CreateRoomReply;
				root2["secure"] = false;
				root2["body"] = "planetside";
				root2["key"] = "";
				t->send(&ioc->soc,&(writer.write(root2))[0u],&ioc->o);
			}
			else if (root["packetType"].asInt() == PacketType::OP_FindRoom) {
				root2["packetType"] = PacketType::OP_FindRoomReply;
				root2["secure"] = false;
				root2["body"] = Room::findRoom(ioc->addr);
				root2["key"] = "";
				t->send(&ioc->soc, &(writer.write(root2))[0u], &ioc->o);
			}
			else if (root["packetType"].asInt() == PacketType::OP_KillRoom) {
				std::string localIp = root["body"].asString();
				root2["packetType"] = PacketType::OP_KillRoomReply;
				root2["secure"] = false;
				root2["body"] = "";
				root2["key"] = "";
				if (Room::killRoom(ioc->addr, localIp) == 0) {
					t->send(&ioc->soc, &(writer.write(root2))[0u], &ioc->o);
				}
				else {
					std::cout << "err" << std::endl;
					t->send(&ioc->soc, &(writer.write(root2))[0u], &ioc->o);
				}
			}
			ioc->mode = 1;
		}
		else {
			ioc->mode = 0;
		}
		t->recv(&ioc->soc, &ioc->buffer, &ioc->o);
	}
}
BasicServer::~BasicServer()
{
}
