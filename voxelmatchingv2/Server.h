#pragma once
#include <iostream>
#include "json\json.h"
#include "UDPSocket.h"
#include "Room.h"
#include "op_code.h"

class Server
{
public:
	Server(UDPSocket *);
	~Server();
#if _WIN32
	UDPSocket *udp;
	static DWORD WINAPI serverFunc(LPVOID);
	static DWORD WINAPI handleData(LPVOID);
	static HANDLE hcp;
#endif
};

