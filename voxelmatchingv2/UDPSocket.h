#pragma once
#if _WIN32
#pragma comment(lib,"ws2_32.lib")
#include <WinSock2.h>
#include <Ws2tcpip.h>
#endif

#include <iostream>
class UDPSocket
{
public:
	UDPSocket(char*,int,int);
	~UDPSocket();
#if _WIN32
	SOCKET soc;
	WSADATA wsaData;
	SOCKADDR_IN addr, clntaddr;
	int sendTo(char *, SOCKADDR_IN);
	int recvFrom(WSABUF *buffer, SOCKADDR_IN *);

#else

#endif
};

