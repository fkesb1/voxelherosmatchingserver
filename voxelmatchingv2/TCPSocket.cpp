#include "TCPSocket.h"



TCPSocket::TCPSocket(char *addr, int port, int mode)
{
#if _WIN32
	if (WSAStartup(MAKEWORD(2, 2), &wsaData)) {
		std::cout << "WSAStartup ERROR" << std::endl;
		exit(-1);
	}
	soc = WSASocket(AF_INET,SOCK_STREAM,IPPROTO_TCP,0,NULL,WSA_FLAG_OVERLAPPED);
	if (soc == INVALID_SOCKET) {
		std::cout << "INVALID SOCKET" << std::endl;
		WSACleanup();
		exit(-1);
	}
	this->addr.sin_family = AF_INET;
	this->addr.sin_port = htons(port);
	wchar_t wtext[20];
	mbstowcs_s(NULL, wtext, addr, strlen(addr) + 1);
	LPWSTR ptr = wtext;
	InetPton(AF_INET, ptr, &(this->addr.sin_addr));
	if (bind(soc, (sockaddr *)&this->addr, sizeof(this->addr))) {
		std::cout << "BIND ERROR" << std::endl;
		WSACleanup();
		exit(-1);
	}
	if (listen(soc, 1024)) {
		std::cout << "LISTEN ERROR" << std::endl;
		WSACleanup();
		exit(-1);
	}
	std::cout << "TCP SOCKET START" << std::endl;
#endif
}
int TCPSocket::send(SOCKET* soc,char *data, OVERLAPPED *o) {
	WSABUF buffer;
	buffer.buf = data;
	buffer.len = sizeof(*data);
	DWORD flag = 0;
	int re = WSASend(*soc, &buffer, 1, &buffer.len, flag, o, NULL);
	//std::cout << WSAGetLastError() << std::endl;
	return re;
}
int TCPSocket::recv(SOCKET* soc, WSABUF* buf,OVERLAPPED *o) {
	DWORD flag = 0;
	int re = WSARecv(*soc, buf, 1, NULL, &flag,o ,NULL);
	//std::cout << WSAGetLastError() << std::endl;
	return re;
}
SOCKET TCPSocket::acceptSocket(SOCKET* soc,SOCKADDR_IN* addr,int *size) {
	SOCKET s= accept(*soc, (SOCKADDR*)addr, size);
	return s;
}
TCPSocket::~TCPSocket()
{

}
