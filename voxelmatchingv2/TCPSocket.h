#pragma once
#if _WIN32
#include <WinSock2.h>
#include <Ws2tcpip.h>
#endif
#include <iostream>
class TCPSocket
{
public:
	TCPSocket(char*,int,int);
	~TCPSocket();
	SOCKET soc;
	int send(SOCKET*,char*, OVERLAPPED *o);
	int recv(SOCKET*,WSABUF*, OVERLAPPED*);
	SOCKET acceptSocket(SOCKET*,SOCKADDR_IN*,int*);
	SOCKADDR_IN addr;
private:
	WSADATA wsaData;
};

