#pragma once
enum PacketType {
	OP_dummy,
	OP_login,
	OP_loginReply,
	OP_CreateRoom,
	OP_CreateRoomReply,
	OP_FindRoom,
	OP_FindRoomReply,
	OP_KillRoom,
	OP_KillRoomReply
};
