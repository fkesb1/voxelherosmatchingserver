#pragma once
#include <iostream>
#include <string>
#include <vector>
#if _WIN32
#include <WinSock2.h>
#endif
class Room {
public:
	Room(SOCKADDR_IN,std::string);
	~Room();
	static std::string findRoom(SOCKADDR_IN);
	static int killRoom(SOCKADDR_IN addr, std::string);
private:
	SOCKADDR_IN addr;
	std::string localAddr;
};